from django.conf import settings
from apps.external_services.lobster.client import LobsterClient
from apps.external_services.lobster.dto import AnonymousUser
from apps.external_services.lobster.settings import CLIENT_KWARGS

import logging


class AddUserToRequest(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        token = request.META.get(settings.JWT_HTTP_HEADER)
        if token and token != 'undefined':
            try:
                client = LobsterClient(**CLIENT_KWARGS)
                user = client.get_user_by_token(token)
                setattr(request, 'lobster_user', user)
            except Exception as e:
                logging.error(e.args)
                setattr(request, 'lobster_user', AnonymousUser())
        else:
            setattr(request, 'lobster_user', AnonymousUser())
        response = self.get_response(request)
        return response
