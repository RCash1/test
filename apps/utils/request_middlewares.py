import json


class JsonParamsToRequest(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        try:
            body = request.body.decode('utf-8')
            json_params = json.loads(body)
            business_type = request.META.get("HTTP_BUSINESS_TYPE", 'ENRG')
        except Exception as e:
            json_params = {}
            business_type = 'ennergiia'
        request.JSON = json_params
        request.business_type = business_type
        response = self.get_response(request)
        if hasattr(response, 'content'):
            response['Content-Length'] = str(len(response.content))
        return response
