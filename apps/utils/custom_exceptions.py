from dataclasses import dataclass, field
from typing import List, Dict

from django.forms import Form

from utils.functions import underscore_to_camel
from .base_dto import Dto


@dataclass
class ErrorDto(Dto):
    code: int = 500
    message: str = 'Неизвестная ошибка'
    errors: List = field(default_factory=list)


class OrderException(Exception):
    status_code = 500
    message = 'Внутренняя ошибка сервиса'
    reason = 'orderException'

    def __init__(self, message=None, errors: List[Dict] = None):
        if message:
            self.message = message
        if errors is None:
            self.errors = [{'reason': self.reason}]
        else:
            self.errors = errors

    def __str__(self):
        return self.message


class HTTPError(OrderException):
    """Ошибка запроса к сервису"""


class FormValidationError(OrderException):
    status_code = 400
    message = 'Переданы невалидные данные'
    reason = 'formValidationError'


class BonusPriceNotAllowedError(OrderException):
    status_code = 400
    message = 'Для этой позиции нет цены за бонусы'
    reason = 'bonusPriceNotAllowedError'


class AddressServiceError(OrderException):
    status_code = 400
    message = 'Не удалось создать адрес по данным'
    reason = 'addressServiceError'


class PaymentsServiceError(OrderException):
    status_code = 500  # TODO: заменить на 4**
    message = 'Ошибка сервиса оплат'
    reason = 'paymentsServiceError'


class DeliveryAddressNotFound(OrderException):
    status_code = 404
    message = 'Не найдены данные адреса по guid'
    reason = 'deliveryAddressNotFound'


class DeliveryDoesNotExist(OrderException):
    status_code = 404
    message = 'Не оформлена доставка'
    reason = 'deliveryNotFound'


class DeliveryNotAvailable(OrderException):
    status_code = 400
    message = 'Доставка в Ваш населённый пункт временно недоступна'
    reason = 'deliveryNotAvailable'


class PickupDoesNotExist(OrderException):
    status_code = 404
    message = 'Не оформлен самовывоз'
    reason = 'pickupNotFound'


class OrderForReturnDoesNotExist(OrderException):
    status_code = 404
    message = 'Нет заказа доступного для возврата'
    reason = 'orderNotFound'


class OrderDoesNotExist(OrderException):
    status_code = 404
    message = 'Заказ не найден'
    reason = 'orderNotFound'


class OrderWithoutGuid(OrderException):
    status_code = 400
    message = 'Не указан guid'
    reason = 'orderWithoutGuid'


class DeliveryCityDoesNotExist(OrderException):
    status_code = 404
    message = 'Город для доставки не найден'
    reason = 'deliveryCityNotFound'


class DeliveryCityMultipleObjectsReturned(OrderException):
    status_code = 400
    message = 'Не удалось получить конкретный город для доставки'
    reason = 'deliveryCityMultipleObjectsReturned'


class OrderIsEmpty(OrderException):
    status_code = 404
    message = 'Позиции заказа были пересены в Ваш wishlist'
    reason = 'orderIsEmpty'


class MultipleDeliveryObjectsReturned(OrderException):
    status_code = 400
    message = 'Не удалось получить конкретную доставку'
    reason = 'multipleDeliveriesFound'


class MultiplePickupObjectsReturned(OrderException):
    status_code = 400
    message = 'Не удалось получить конкретную информацию о самовывозе'
    reason = 'multiplePickupsFound'


class MultipleOrderObjectsReturned(OrderException):
    status_code = 400
    message = 'Не удалось получить конкретный заказ'
    reason = 'multipleOrdersFound'


class ProductReturnError(OrderException):
    status_code = 400
    message = 'Товар недоступен для возврата'
    reason = 'productReturnError'


class ProductAlreadyExist(OrderException):
    status_code = 400
    message = 'Данный товар уже находится в корзине'
    reason = 'productAlreadyExists'


class AuthorizationRequired(OrderException):
    status_code = 401
    message = 'Метод требует авторизации'
    reason = 'authorizationRequired'


class ProductCountExceeded(OrderException):
    status_code = 400
    message = 'Превышено количество доступного товара'
    reason = 'productCountExceeded'


class DeliveryCalculationMissed(OrderException):
    status_code = 400
    message = 'Расчёт доставки был пропущен, невозможно добавить доставку'
    reason = 'deliveryCalculationMissed'


class EmptyDeliveryAddress(OrderException):
    status_code = 400
    message = 'Адрес доставки не может быть пустой'
    reason = 'emptyDeliveryAddress'


class EmptyDeliveryDate(OrderException):
    status_code = 400
    message = 'Дата доставки не может быть пустой'
    reason = 'emptyDeliveryDate'


class OverdueDeliveryDate(OrderException):
    status_code = 400
    message = 'Дата доставки не может быть сегодняшним или предыдущим днём'
    reason = 'overdueDeliveryDate'


class ProductDoesNotExist(OrderException):
    status_code = 404
    message = 'Продукт с данным кодом не найден'
    reason = 'productDoesNotExist'


class BTLTokenError(OrderException):
    status_code = 400
    message = 'Невалидный токен'
    reason = 'BTLTokenError'


class PaymentMethodDoesNotExist(OrderException):
    status_code = 404
    message = 'Указанный способ оплаты не найден'
    reason = 'paymentMethodDoesNotExist'


class PaymentMethodIsNotChosen(OrderException):
    status_code = 400
    message = 'Не выбран метод оплаты'
    reason = 'PaymentMethodIsNotChosen'


class OrderInCheckout(OrderException):
    status_code = 403
    message = 'Заказ уже оформлен'
    reason = 'orderInCheckout'


class PositionNotStock(OrderException):
    status_code = 400
    message = 'Товара нет на остатках'
    reason = 'positionNotInStock'


class OrgStructureServiceError(OrderException):
    status_code = 500  # TODO: заменить на 4**
    message = 'API оргструктуры вернул ошибку'
    reason = 'orgStructureApiError'


class CatalogServiceError(OrderException):
    status_code = 500  # TODO: заменить на 4**
    message = 'API каталога вернул ошибку'
    reason = 'catalogServiceError'


class AvailabilityServiceError(OrderException):
    status_code = 400
    message = 'API доступности вернул ошибку'
    reason = 'availabilityServiceError'


class ErrorPromo(OrderException):
    status_code = 500  # TODO: заменить на 4**
    message = 'Внутренняя ошибка сервиса промо'
    reason = 'errorPromo'


class ErrorPromoOrderId(ErrorPromo):
    status_code = 404
    message = 'Заказ не найден'

    def __init__(self, message=message, errors=None, guid=None):
        super().__init__(message='{0}: {1}'.format(message, guid), errors=errors)


class ErrorPromoCodeNotFound(ErrorPromo):
    status_code = 404
    message = 'Промокод не найден'

    def __init__(self, message=message, errors=None, promo_code=None):
        super().__init__(message='{0}: {1}'.format(message, promo_code), errors=errors)


class IncorrectFileFormatException(OrderException):
    status_code = 500
    message = 'Формат файла неверный'
    reason = 'incorrectFileFormatException'


class RevoException(PaymentsServiceError):
    status_code = 400
    message = 'Ошибка от РЕВО+'
    reason = 'revoException'


class RevoCityException(RevoException):
    status_code = 400
    message = 'В Вашем городе оплата частями временно недоступна'
    reason = 'revoCityException'


class RevoLimitException(RevoException):
    status_code = 400
    message = 'Сумма заказа превышает доступный лимит оплаты частями'
    reason = 'revoLimitException'


class OrderCantBeCancelled(OrderException):
    status_code = 400
    message = 'Заказ не может быть отменён автоматически. Свяжитесь с колл-центром магазина.'
    reason = 'orderCantBeCancelled'


class ServiceUnavailable(HTTPError):
    pass


class ErpServiceUnavailable(ServiceUnavailable):
    message = 'SAP не отвечает'
    reason = 'serviceUnavailable'


class NoDeliveryInfo(OrderException):
    status_code = 400
    message = 'Заказ не имеет информации о способе доставки.'
    reason = 'noDeliveryInfo'


class DeliveryIsNotAllowed(OrderException):
    status_code = 500
    message = 'Метод доставки запрещен для выбранного города'
    reason = 'deliveryMethodIsNotAllowed'


class PaymentIsNotAllowed(OrderException):
    status_code = 500
    message = 'Выбранный метод отплаты запрещен для выбранного города'
    reason = 'paymentMethodIsNotAllowed'


class WrongHerbloyaltyToken(OrderException):
    """Неверный токен сервиса лояльности гербам"""

    status_code = 403
    message = 'Неверный токен сервиса лояльности гербам'
    reason = 'wrongHerbloyaltyToken'


class BaseError(Exception):
    status_code = 500
    message = 'Неизвестная ошибка'
    errors = []
    field = ''

    def __init__(self, message='', field: str = '', errors: list = []):
        if message:
            self.message = message
        if field:
            self.field = field
        if errors:
            self.errors = errors

    def __str__(self):
        return str(self.message)


class ValidationError(BaseError):
    status_code = 400
    message = 'Переданы невалидные данные'


class AggregatorHttpExceptions(BaseError):
    def __init__(self, status_code: int = 500, message: str = None, errors: list = None, *args):
        self.exceptions = errors
        self.status_code = status_code
        if message:
            self.message = message
        super().__init__(*args)


def raise_form_validation_error(form: Form):
    errors = [ValidationError(field=underscore_to_camel(field), message=error[0])
              for field, error in form.errors.items()]
    raise AggregatorHttpExceptions(errors=errors, status_code=400)


class DeliveryDateInPast(OrderException):
    """Дата доставки в прошлом"""

    status_code = 500
    message = "Дата доставки не можеть быть в прошлом. Измените дату доставки"
    reason = "deliveryDateInPast"
