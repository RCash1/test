from datetime import datetime
from typing import Optional, List

import inflection
from pydantic import BaseModel, Field


class VideoDto(BaseModel):
    uuid: str
    video: Field(alias='url')
    username: Optional[str]
    last_name: Optional[str]
    created_date: datetime
    views: int = 0
    hashtags: List[Optional[str]] = []
    product_id: Optional[int]

    class Config:
        alias_generator = inflection.camelize
        orm_mode = True

