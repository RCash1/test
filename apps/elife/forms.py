from django import forms

from apps.elife.models import VideoModel


class VideoPostForm(forms.ModelForm):
    class Meta:
        model = VideoModel
        fields = ['video', 'hashtags', 'user']
