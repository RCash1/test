from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from apps.elife.views import VideoView, VideoListView, VideoViewsCountView

urlpatterns = [
    path('', csrf_exempt(VideoView.as_view()), name='video-upload'),
    path('<str:video_id>', VideoView.as_view(), name='video-view'),
    path('list/', VideoListView.as_view(), name='video-list-view'),
    path('viewed/', VideoViewsCountView.as_view(), name='video-views-count'),
]
