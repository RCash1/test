from django.db.models import Q, Count

from apps.elife.constants import ORDERING_TYPES
from apps.elife.dtos import VideoDto
from apps.elife.models import VideoModel, VideoViewModel
from apps.elife.schema import ListRequest, VideoViewRequest


class VideosService:

    @staticmethod
    def get_videos(params: ListRequest, user_id: int = None):
        filter_params = params.dict(by_alias=True, exclude_none=True,
                                    exclude={'sort_by', 'own'})
        if params.own:
            filter_params.update(user=user_id)
        videos = VideoModel.objects\
            .filter(**filter_params)#.annotate(vviews=Count('views'))
        if params.sort_by:
            videos.order_by(ORDERING_TYPES[params.sort_by])
        return [VideoDto.from_orm(video) for video in videos]


class VideoViewsService:
    @staticmethod
    def save_view(params: VideoViewRequest, user_id: int = None):
        VideoViewModel.objects.create(
            video_id=params.video,
            user=user_id
        )
