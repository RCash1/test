import uuid as uuid
from cloudinary.models import CloudinaryField
from django.db import models
from django.contrib.postgres.fields import ArrayField


class VideoModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    uuid = models.CharField(max_length=36, auto_created=uuid.uuid4)
    user = models.PositiveIntegerField(blank=True, null=True)
    video = CloudinaryField('video')
    hashtags = ArrayField(
        models.CharField(max_length=50, blank=True)
    )

    class Meta:
        db_table = 'elife_video'
        verbose_name_plural = 'videos'
        verbose_name= 'video'


class VideoViewModel(models.Model):
    video = models.ForeignKey(VideoModel, related_name='views', on_delete=models.CASCADE)
    user = models.PositiveIntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'elife_videoview'
        verbose_name_plural = 'video_views'
        verbose_name = 'video_view'
