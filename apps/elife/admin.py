from django.contrib import admin

from apps.elife.models import VideoModel


@admin.register(VideoModel)
class VideoAdmin(admin.ModelAdmin):
    model = VideoModel
