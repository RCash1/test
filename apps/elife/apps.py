from django.apps import AppConfig


class ElifeConfig(AppConfig):
    name = 'apps.elife'
