from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View

from apps.elife.services import VideosService, VideoViewsService
from apps.elife.forms import VideoPostForm
from apps.elife.schema import ListRequest, VideoViewRequest


class VideoView(View):
    def get(self, request):
        form = VideoPostForm()
        return render(request, 'upload_form.html', context={'form': form})

    def post(self, request):
        form = VideoPostForm(request.POST, request.FILES)
        if form.is_valid():
            form.user = 123
            form.save()
        return redirect(reverse('video-list-view'), )


class VideoListView(View):
    def get(self, request):
        params = ListRequest.parse_obj(request.GET)
        videos = VideosService.get_videos(params)
        return render(request, reverse('video-list-view'), context={
            'videos': videos
        })


class VideoViewsCountView(View):

    def post(self, request):
        params = VideoViewRequest.parse_obj(request.POST)
        VideoViewsService.save_view(params)
        return True
