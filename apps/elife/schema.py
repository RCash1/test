import inflection
from pydantic import BaseModel, Field

from typing import Optional


class ListRequest(BaseModel):
    product_id: Optional[int]
    own: Optional[bool]
    sort_by: Optional[str]
    # hashtags: Field(None, alias="hashtags")

    class Config:
        alias_generator = inflection.underscore


class VideoViewRequest(BaseModel):
    video: str

