import logging
import ujson
from django.conf import settings
from django.core.cache import cache

from apps.external_services.client import BaseClient
from apps.external_services.lobster.factory import UserDtoFactory

__all__ = ('LobsterClient',)
logger = logging.getLogger(__name__)


class LobsterClient(BaseClient):
    headers = {"Content-Type": "application/json", "referrer": settings.AUTH_API_REFERER, "x-language": "ru",
               "service-slug": "omni-order", 'x-access-token': ''}

    def get_user_by_token(self, token):
        """Получение пользователя по токену"""
        headers = self.headers.copy()
        headers['x-access-token'] = token
        cache_data = cache.get(token)
        if cache_data and settings.LOBSTER_CACHE_ENABLED:
            return UserDtoFactory.dto_from_cache(cache_data)
        else:
            response = self._post('/login', headers=headers)
            user = UserDtoFactory.dto_from_dict(response)
            cache.set(token, user.to_dict(use_snake_case=True), settings.LOBSTER_CACHE_TIME)
            return user

    def get_user_by_pk(self, pk):
        """Получение авторизации пользователя по pk"""
        cache_data = cache.get(pk)
        if cache_data and settings.LOBSTER_CACHE_ENABLED:
            return UserDtoFactory.dto_from_cache(cache_data)
        else:
            token = self.microservice_login()
            response = self._post('/forced-login',
                                  headers=self.headers,
                                  data=ujson.dumps({
                                      'sap_code': str(pk),
                                      'mtoken': token
                                  }))
            user = UserDtoFactory.dto_from_dict(response)
            cache.set(pk, user.to_dict(use_snake_case=True), settings.LOBSTER_CACHE_TIME)
            return user

    def microservice_login(self):
        """Авторизация микросервиса"""
        response = self._post('/microservice-auth',
                              headers=self.headers,
                              data=ujson.dumps({
                                  'username': settings.AUTH_API_LOGIN,
                                  'password': settings.AUTH_API_PASSWORD
                              }))
        return response['meta']['token']
