from external_services.lobster.dto import UserDto


class UserDtoFactory:

    @classmethod
    def dto_from_dict(cls, item_dict):
        return UserDto(
            id=item_dict['data']['attributes'].get('user_id'),
            token=item_dict['meta'].get('token'),
            phone=item_dict['data']['attributes'].get('phone'),
            name=item_dict['data']['attributes'].get('first_name'),
            is_active=item_dict['data']['attributes'].get('is_active')
        )

    @classmethod
    def dto_from_cache(cls, cache_item):
        return UserDto(**cache_item)
