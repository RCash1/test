from dataclasses import dataclass
from utils.base_dto import Dto


@dataclass
class UserDto(Dto):
    id: int
    token: str
    phone: str
    name: str = None
    is_active: bool = True


class AnonymousUser(Dto):
    id = 0
    token = None
    phone = None
    name = ''
    is_active: bool = True
