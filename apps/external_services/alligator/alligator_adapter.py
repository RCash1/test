import logging
from typing import Dict, Union

import requests
from django.conf import settings
from django.core.cache import cache

from apps.utils import custom_exceptions


CACHE_KEY_ALLIGATOR_DATA = 'alligator_position_data_{product_item_id}_{svyabk}'

logger = logging.getLogger(__name__)


def update_cache_alligator(item_id, svyabk, product):
    if settings.USE_CACHE_FOR_ALLIGATOR:
        cache_key = CACHE_KEY_ALLIGATOR_DATA.format(product_item_id=item_id, svyabk=svyabk)
        cache.set(cache_key, product, timeout=settings.CACHE_TTL)


def prepare_position_for_alligator(position):
    return {
        'productItemId': str(position['productItemId']),
        'quantity': position['quantity'],
        'unitId': position.get('unitId', position.get('measureUnit'))
    }


def send_check_basket(body: dict, token: str = None, user_id: int = None):
    try:
        if token:
            headers = {"Authorization": f"Bearer {token}"}
            return requests.post(settings.ALLIGATOR_URL + '1/check-basket',
                                 headers=headers, json=body, timeout=5)
        elif user_id:
            return requests.post(settings.ALLIGATOR_URL + '1/check-basket',
                                 params={"userId": user_id}, json=body, timeout=5)
        else:
            return requests.post(settings.ALLIGATOR_URL + '1/check-basket', json=body, timeout=5)
    except Exception as e:
        logging.info('Аллигатор недоступен {}'.format(e))
        logging.exception(e)
        raise custom_exceptions.CatalogServiceError


def get_basket_info(body: dict, token: str, product_id: str = None, user_id: int = None):
    response = send_check_basket(body=body, token=token, user_id=user_id)
    if response.ok:
        try:
            json_body = response.json()
            # пока идёт отладка на v2, лучше иметь эти логи
            logging.debug("Ответ от Аллигатора: {}".format(json_body))
            json_data = json_body["data"]
            # body_items = [item['productItemId'] for item in body['productItems']]
            items = []
            response = {}
            for product in json_data['items']:
                product_item = _get_product_item(product, product_id)
                if product_item:
                    item_id = product_item.get("productItemId")
                    if settings.USE_CACHE_FOR_ALLIGATOR:
                        update_cache_alligator(item_id, body['svyabk'], product)
                    prices = product_item.get('prices')
                    product = {
                        "productId": product["productId"],
                        "name": product["productName"],
                        "modifiers": product["modifiers"],
                        "productItemId": item_id,
                        "prices": prices[0] if prices else None,
                        "measureUnit": prices[0]["unitId"] if prices else None,
                        "available": product_item["availability"]["isAvailable"],
                        "leadtime": product_item["availability"]["leadTime"],
                        "quantity": body.get("productItems")[0].get("quantity"),
                        "bonusPrice": product.get('productItems', [{}])[0].get('bonusPrice')
                    }
                    items.append(product)
            response['availability'] = json_data['availability']
            response['items'] = items
            return response
        except Exception as e:
            logging.info("Ответ каталога не обработан {}".format(e))
            logging.exception(e)
            raise e
    else:
        try:
            error_data = response.json()
        except ValueError:
            error_data = response
        logging.exception("Каталог вернул ответ с ошибкой: {}. Данные request: {}.".format(error_data, body))
        raise custom_exceptions.CatalogServiceError


def _get_product_item(product: Dict, product_id: str) -> Union[Dict, None]:
    """Получение товара из ответа аллигатора по productId.
    Сделано на случай появления одинакового productItemId в нескольких productItems."""
    if not product_id or int(product_id) == product.get('productId'):
        return product["productItems"][0]
    logger.exception(msg='Ошибка в данных каталога, '
                     'в артикуле присутствуют товары разных артикулов',
                     extra={"productId": product_id,
                            "product": product})
    return None
